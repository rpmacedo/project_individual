var address;

function initMap() {
	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 16,
		center: {lat: 38.7092147, lng: -9.1430909}
	});
	var geocoder = new google.maps.Geocoder();

	document.getElementById('pesquisar').addEventListener('click', function() {
		$.ajax({
			type: "GET",
			url:"../rest/maps/" + document.getElementById('user').value,
			contentType: "application/json; charset=utf-8",
			crossDomain: true,
			dataType: "json",
			success: function (response) {
				if (response) {
					var data = new Date();
					var now = data.getTime();
					var token = localStorage['tokenExpiration'];
					var valid = token - now;

					if (token) {
						if (valid < 0) {
							alert ("You're not logged in");
							location.href ="/index.html";
						} else {
							address = response;
							geocodeAddress(geocoder, map, address);
						}
					} else {
						alert ("You're not logged in");
						location.href ="/index.html";
					}

				} else {
					alert ("No response");
				}
			},
			error: function (response) {
				alert("Error: " + response.status);
			},   
		});
	});
	
		document.getElementById('logout').addEventListener('click', function () {
		$.ajax({
			type: "GET",
			url:"../rest/logout",
				contentType: "application/json; charset=utf-8",
				crossDomain: true,
				dataType: "json",
				success: function (response) {
					if (response) {
						alert ("Logged Out");
						localStorage.removeItem('tokenID');
						location.href="/index.html";
					} else {
						alert ("No response");
					}
				},
				error: function (response) {
					alert("Error: " + response.status);
				},   
		});
	});
}
function geocodeAddress(geocoder, resultsMap, address) {
	geocoder.geocode({'address': address}, function(results, status) {
		if (status === google.maps.GeocoderStatus.OK) {
			resultsMap.setCenter(results[0].geometry.location);
			var marker = new google.maps.Marker({
				map: resultsMap,
				position: results[0].geometry.location
			});
		} else {
			alert('Geocode was not successful for the following reason: ' + status);
		}
	});
}
