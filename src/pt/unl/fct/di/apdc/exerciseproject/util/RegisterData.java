package pt.unl.fct.di.apdc.exerciseproject.util;

public class RegisterData {

	public String username;
	public String name;
	public String email;
	public String password;
	public String confirmation;
	public String rua;
	public String complementar;
	public String localidade;
	public String cp;
	public String nif;
	public String cc;
	public String fixo;
	public String movel;
	
	public RegisterData() {};
	
	public RegisterData (String username,String name, String email, String password, String confirmation, String rua, 
			String complementar, String localidade, String cp, String nif, String cc, String fixo, String movel) {
		this.username = username;
		this.name = name;
		this.email = email;
		this.password = password;
		this.confirmation = confirmation;
		this.rua = rua;
		this.complementar = complementar;
		this.localidade = localidade;
		this.cp = cp;
		this.nif = nif;
		this.cc = cc;
		this.fixo = fixo;
		this.movel = movel;
	}
	private boolean validField (String value) {
		return value !=null && !value.equals("");
	}
	
	/*private boolean validNumber (int value) {
		return value > 99999999 && value <= 999999999;
	}*/
	
	public boolean validRegistration() {
		return validField(username) && validField (name) && validField (email) && 
				validField(password) && validField (confirmation) && validField(rua) &&
				validField (cp) && validField (fixo) && validField (movel) &&
				validField (nif) && validField (cc) && password.equals(confirmation) && email.contains("@");
	}
	
}
